// TODO: movable player
// TODO: player firing
// TODO: asteroids
// TODO: asteroid generator
// TODO: collision detection
// TODO: asteroid division
// TODO: player lives
// TODO: player scores

//#![allow(clippy::unnecessary_wraps)]

use ggez::{Context, ContextBuilder, GameResult};
use ggez::conf;
use ggez::event::{self};
use ggez::glam::*;
use ggez::graphics::{self};
//use ggez::input::keyboard::{KeyCode, KeyInput};

// include gameobject.rs
mod gameobject;

const WINDOW_WIDTH:  f32 = 500.0;
const WINDOW_HEIGHT: f32 = 500.0;


struct Game {
    player: gameobject::GameObject
}

impl Game {
    fn new(ctx: &mut Context) -> GameResult<Game> {
        let g = Game
        {
            player: gameobject::GameObject::new(ctx)?
        };

        Ok(g)
    }
}

impl event::EventHandler<ggez::GameError> for Game {
    // update loop
    fn update(&mut self, _ctx: &mut Context) -> GameResult {
        self.player.position.x = self.player.position.x % 800.0 + 1.0;
        Ok(())
    }

    // draw loop
    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        let mut canvas =
            graphics::Canvas::from_frame(ctx, graphics::Color::from([0.1, 0.2, 0.3, 1.0]));

        canvas.draw(&self.player.mesh, self.player.position);

        canvas.finish(ctx)?;
        Ok(())
    }
}

pub fn main() -> GameResult {
    let cb = ContextBuilder::new("asteroids", "")
        .window_setup(conf::WindowSetup::default().title("Asteroids"))
        .window_mode(conf::WindowMode::default().dimensions(WINDOW_WIDTH, WINDOW_HEIGHT));

    let (mut ctx, event_loop) = cb.build()?;
    let state = Game::new(&mut ctx)?;

    event::run(ctx, event_loop, state)
}

