use ggez::{Context, GameResult};
use ggez::glam::{Vec2, vec2};
use ggez::graphics::{self, Color};

pub struct GameObject
{
    pub position: Vec2,
    pub mesh: graphics::Mesh
}

impl GameObject
{
    pub fn new(ctx: &mut Context) -> GameResult<GameObject>
    {
        let mesh = graphics::Mesh::new_circle(
            ctx,
            graphics::DrawMode::fill(),
            vec2(0., 0.),
            100.0,
            2.0,
            Color::WHITE,
        )?;

        let position = Vec2
        {
            x: 50.0f32,
            y: 50.0f32
        };

        let object = GameObject
        {
            position,
            mesh
        };

        Ok(object)
    }

}

